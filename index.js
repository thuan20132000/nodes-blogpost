
const express = require('express')
const app = express()
const path = require('path')
const ejs = require('ejs')
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const fileUpload = require('express-fileupload');
const expressSession = require('express-session');
const flash = require('connect-flash');

// Model
const BlogPost = require('./models/BlogPost');

//Controller
const homeController = require('./controller/home');
const getPostController = require('./controller/getPost');
const createPostController = require('./controller/newPost');
const storePostController = require('./controller/storePost');
const newUserController = require('./controller/newUser');
const storeUserController = require('./controller/storeUser')
const loginController = require('./controller/login')
const loginUserController = require('./controller/loginUser');
const logoutUserController = require('./controller/logoutUser');

// Middleware
const validateMiddleware = require('./middleware/validationMiddleware');
const authMiddleware = require('./middleware/authMiddleware');
const redirectIfAuthenticatedMiddleware  = require('./middleware/redirectIfAuthenticatedMiddleware');



app.set('view engine','ejs')
app.use(express.static('public'))
app.use(bodyParser.urlencoded({extended:true}))
app.use(fileUpload())
app.use(expressSession({
    secret:'keyboardcat'
}))
app.use(flash());

mongoose.connect('mongodb+srv://admin:adminadmin@cluster0.9smnn.mongodb.net/my_database',{useNewUrlParser:true});

global.loggedIn = null;
let port = process.env.PORT;

if(port == null || port == ""){
    port = 4000;
}

app.listen(port,() => {
    console.log('App listening on port 4000')
})

app.use('*',(req,res,next) => {
    loggedIn = req.session.userId;
    next();
})



app.get('/',homeController)
app.get('/post/:id',getPostController)
app.get('/posts/new',authMiddleware,createPostController)
app.post('/post/store',validateMiddleware,storePostController)

app.get('/auth/register',redirectIfAuthenticatedMiddleware,newUserController);
app.post('/users/register',redirectIfAuthenticatedMiddleware,storeUserController);

app.get('/auth/login',redirectIfAuthenticatedMiddleware,loginController);
app.post('/users/login',redirectIfAuthenticatedMiddleware,loginUserController);

app.get('/auth/logout',logoutUserController);

app.use((req,res) => res.render('notfound'));